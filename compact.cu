// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>


//Thrust
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <thrust/unique.h>
#include <thrust/transform_scan.h>

//My defines
#include <defines.h>

struct lessByV1
{
  __host__ __device__
  bool operator()(const Edge &x, const  Edge &y)
  {
      return x.v1 < y.v1;
  }
};


struct lessByV2
{
  __host__ __device__
  bool operator()(const Edge &x, const  Edge &y)
  {
      return x.v2 < y.v2;
  }
};

struct eqByV1andV2 
{
  __host__ __device__
  bool operator()(const Edge &x, const  Edge &y)
  {
      return x.v1 == y.v1 && x.v2 == y.v2;
  }
};


struct toUint
{
  __host__ __device__
  uint operator()(bool b)
  {
      return uint(b);
  }
};

struct isSingle
{
  __host__ __device__
  bool operator() (const Edge &e)
  {
      return e.isSingle;
  }
};

struct isInvalid
{
  __host__ __device__
  bool operator() (const Edge &e)
  {
      return !e.isValid;
  }
};

void __global__
fixIndices(Triangle *g_tris, uint *g_newIndices, uint numTris) {
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numTris) {
        return;
    }
    Triangle t = g_tris[idx]; 
    Triangle n = {g_newIndices[t.v1], g_newIndices[t.v2], g_newIndices[t.v3]};
    g_tris[idx] = n;
}

void __global__
findSingleEdges(Edge *g_edges, uint numEdges) {
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }
    Edge e = g_edges[idx],
         eprev = g_edges[(idx > 0) ? idx-1 : idx + 1],
         enext = g_edges[(idx < numEdges - 1) ? idx+1 : idx - 1];
    g_edges[idx].isSingle = !(e.v1 == eprev.v1 && e.v2 == eprev.v2) &&
                 !(e.v1 == enext.v1 && e.v2 == enext.v2);
}

extern "C"
uint 
compactEdges(Edge *d_edges, uint numEdges, uint threadsPerBlock){
    thrust::device_ptr<Edge> d_edges_dev = thrust::device_pointer_cast<Edge>(d_edges); 
    thrust::sort(d_edges_dev, d_edges_dev + numEdges, lessByV1());
    thrust::stable_sort(d_edges_dev, d_edges_dev + numEdges, lessByV2());

    //uint blocksInGrid = getBlocksInGrid(numEdges, threadsPerBlock);
    //dim3 grid(blocksInGrid,1,1);
    //dim3 threads(threadsPerBlock,1,1);
    //findSingleEdges<<<grid, threads>>>(d_edges, numEdges);

    //uint singleCount = thrust::count_if(d_edges_dev, d_edges_dev + numEdges, isSingle());
    //std::cout << "Signle edges count: " << singleCount << std::endl;

    return thrust::unique(d_edges_dev, d_edges_dev + numEdges, eqByV1andV2()) - d_edges_dev;
}

extern "C"
uint 
compactInvalidEdges(Edge *d_edges, uint numEdges){
    thrust::device_ptr<Edge> d_edges_dev = thrust::device_pointer_cast<Edge>(d_edges); 
    uint validNum = (thrust::remove_if(d_edges_dev, d_edges_dev + numEdges,  isInvalid()) - d_edges_dev);
    thrust::sort(d_edges_dev, d_edges_dev + validNum, lessByV1());
    thrust::stable_sort(d_edges_dev, d_edges_dev + validNum, lessByV2());
    return thrust::unique(d_edges_dev, d_edges_dev + validNum, eqByV1andV2()) - d_edges_dev;
}

extern "C"
void 
compactFacesAndVertices(Triangle *d_tris, uint numTris, Pos *d_pos,  uint numVerts,
                        bool *d_isTriangleValid, bool *d_isVertexValid,
                        uint &outNewTris, uint &outNewVerts,
                        uint threadsPerBlock) 
{
    thrust::device_ptr<Triangle> d_tris_dev(d_tris);
    thrust::device_ptr<Pos> d_pos_dev(d_pos);
    thrust::device_ptr<bool> d_isVertexValid_dev(d_isVertexValid);
    thrust::device_ptr<bool> d_isTriangleValid_dev(d_isTriangleValid);

    outNewTris = thrust::remove_if(d_tris_dev, d_tris_dev + numTris, d_isTriangleValid_dev, thrust::logical_not<bool>()) - d_tris_dev;

    thrust::plus<uint> plus;
    thrust::device_vector<uint> newIndices(numVerts);
    thrust::transform_exclusive_scan(d_isVertexValid_dev, d_isVertexValid_dev + numVerts, newIndices.begin(),
                                     toUint(), 0U, plus);
    
    uint blocksInGrid = getBlocksInGrid(numTris, threadsPerBlock);
    dim3 grid(blocksInGrid,1,1);
    dim3 threads(threadsPerBlock, 1,1);
    fixIndices<<<grid, threads>>>(d_tris, newIndices.data().get(), numTris);
    checkCudaErrors(cudaGetLastError());

    outNewVerts = thrust::remove_if(d_pos_dev, d_pos_dev + numVerts, d_isVertexValid_dev, thrust::logical_not<bool>()) - d_pos_dev;
}