// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>

#include <thrust/sequence.h>
#include <thrust/device_ptr.h>

#include <defines.h>

void __global__
initCollapseTargets(uint *g_collapseTargets, uint numVerts){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numVerts) {
        return;
    }
    g_collapseTargets[idx] = idx;
}

void __global__
setEdgeIDs(Edge *g_edges, bool *g_willCollapseEdge, float *g_edgeCost, float *g_vertexCost, uint *g_edgeIDs, uint numEdges){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }
    if (!g_willCollapseEdge[idx]) return;

    Edge e = g_edges[idx];
    float edgeCost = g_edgeCost[idx];
    if (g_vertexCost[e.v1] == edgeCost) {
        g_edgeIDs[e.v1] = idx;
    }
    if (g_vertexCost[e.v2] == edgeCost) {
        g_edgeIDs[e.v2] = idx;
    }
}

void __global__
collapse(Pos *g_pos, Edge *g_edges, Pos *g_optimumPos, bool *g_willCollapseEdge, uint *g_edgeIDs, Quadric *g_quadrics, uint *g_collapseTargets, bool *g_isVertexValid, uint numEdges) {
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }
    if (!g_willCollapseEdge[idx]) return;

    Edge e = g_edges[idx];
    if (g_edgeIDs[e.v1] == idx && g_edgeIDs[e.v2] == idx){
        Quadric q1 = g_quadrics[e.v1],
                q2 = g_quadrics[e.v2];
        q1 += q2;
        g_quadrics[e.v1] = q1;
        g_collapseTargets[e.v2] = e.v1;
        g_isVertexValid[e.v2] = false;
        g_pos[e.v1] = g_optimumPos[idx];
    }
}


extern "C"
void collapseEdges(Pos *d_pos, Edge *d_edges, uint numEdges, bool *d_willCollapseEdge, Quadric *d_quadrics, uint numVerts, Pos *d_optimumPos, 
                       float *d_edgeCost, float *d_vertexCost, uint *d_collapseTargets, bool *d_isVertexValid,
                       uint *d_edgeIDs, uint threadsPerBlock){

    uint blocksInGrid = getBlocksInGrid(numVerts, threadsPerBlock);
    dim3 grid(blocksInGrid,1,1);
    dim3 threads(threadsPerBlock,1,1);
    initCollapseTargets<<<grid, threads>>>(d_collapseTargets, numVerts);
    checkCudaErrors(cudaGetLastError());

    blocksInGrid = getBlocksInGrid(numEdges, threadsPerBlock);
    grid = dim3(blocksInGrid, 1,1);
    setEdgeIDs<<<grid, threads>>>(d_edges, d_willCollapseEdge, d_edgeCost, d_vertexCost, d_edgeIDs, numEdges);
    checkCudaErrors(cudaGetLastError());
    
    
    collapse<<<grid, threads>>>(d_pos, d_edges, d_optimumPos, d_willCollapseEdge, d_edgeIDs, d_quadrics, d_collapseTargets, d_isVertexValid, numEdges);
    checkCudaErrors(cudaGetLastError());
}