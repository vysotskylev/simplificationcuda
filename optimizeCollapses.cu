// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>

//Thrust
#include <thrust/fill.h>
#include <thrust/device_ptr.h>

//My defines
#include <defines.h>

const float __device__ eps = 1e-8;

inline bool __device__
epsEq0(float a) {
    return a < eps && a > -eps;
}

bool inline __device__ 
optimize(Quadric &q, Pos &outPos) {
    float D1 = q(0,0);
    if (epsEq0(D1)) {
        return false;
    }
    float L21 = q(1,0) / D1,
          D2 = q(1,1) - L21*L21*D1;
    if (epsEq0(D2)){
        return false;
    }
    float L31 = q(2,0) / D1,
          L32 = (q(2,1) - L31*L21*D1) / D2,
          D3 = q(2,2) - L31*L31*D1 - L32*L32*D2;
    if (epsEq0(D3)){
        return false;
    }
    float d1 = 1.0f / D1,
          d2 = 1.0f / D2,
          d3 = 1.0f / D3;

    float K21 = -L21,
          K31 = -L31 + L21*L32,
          K32 = -L32;
    float M11 = d1 + d2*K21*K21 + d3*K31*K31,
          M21 = d2*K21 + d3*K31*K32,
          M31 =  K31*d3,
          M22 = d2 + d3*K32*K32,
          M32 = d3*K32,
          M33 = d3;
    outPos.x = - (M11 * q(3,0) + M21 * q(3,1) + M31 * q(3,2));
    outPos.y = - (M21 * q(3,0) + M22 * q(3,1) + M32 * q(3,2));
    outPos.z = - (M31 * q(3,0) + M32 * q(3,1) + M33 * q(3,2));
    return true;
}

float inline __device__
calcError(const Quadric &q, const Pos &p){
    return p.x*p.x*q(0,0) + 2*p.x*p.y*q(1,0) + 2*p.x*p.z*q(2,0) + 2*p.x*q(3,0) +
           p.y*p.y*q(1,1) + 2*p.y*p.z*q(2,1) + 2*p.y*q(3,1) + 
           p.z*p.z*q(2,2) + 2*p.z*q(3,2) +
           q(3,3);
}

// Atomic minimum for floats
// WARNING! Works only for nonnegative floats
void inline __device__
fatomicMin(float *adr, float x){
    assert(sizeof(int) == sizeof(float));
    atomicMin((int *) adr, *(int *)(&x));
}

void __global__ kernel(Pos *g_pos, Edge *g_edges, Quadric *g_quadrics, Pos *g_optimumPos,
                       float *g_edgeCost, float *g_vertexCost, float maxError, bool *g_willCollapseEdge, uint numEdges){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }

    Edge e = g_edges[idx];
    if (!e.isValid) {
        return;
    }
    Quadric q1 = g_quadrics[e.v1],
            q2 = g_quadrics[e.v2];
    q1 += q2;
    Pos optPos;
    float optErr;
    if (optimize(q1, optPos)) {
        optErr = calcError(q1, optPos);
    } else {
        Pos p1 = g_pos[e.v1],
            p2 = g_pos[e.v2];
        float err1 = calcError(q1, p1),
              err2 = calcError(q1, p2);
        optPos = (err1 < err2) ? p1 : p2;
        optErr = MIN(err1, err2);
    }
    if (optErr > maxError) {
        // Too big error
        return;
    }
    
    g_willCollapseEdge[idx] = true;
    g_optimumPos[idx] = optPos;
    g_edgeCost[idx] = optErr;
    
    
    fatomicMin(&g_vertexCost[e.v1], optErr);
    fatomicMin(&g_vertexCost[e.v2], optErr);
}

extern "C"
void optimizeCollapses(Pos *d_pos, Edge *d_edges, uint numEdges,  Quadric *d_quadrics, Pos *d_optimumPos, 
                       float *d_edgeCost, float *d_vertexCost, float maxError, bool *d_willCollapseEdge, uint threadsPerBlock){
    
    uint blocksInGrid = getBlocksInGrid(numEdges, threadsPerBlock);
    dim3 grid(blocksInGrid,1,1);
    dim3 threads(threadsPerBlock,1,1);
    kernel<<<grid, threads>>>(d_pos, d_edges,  d_quadrics, d_optimumPos, d_edgeCost, d_vertexCost, maxError, d_willCollapseEdge, numEdges);
    checkCudaErrors(cudaGetLastError());
}

extern "C"
void fillArray(float *d_array, uint numEls, float value){
    thrust::device_ptr<float> d_array_dev(d_array);
    thrust::fill(d_array_dev, d_array_dev + numEls, value);
}