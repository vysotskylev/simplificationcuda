// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>

#include <defines.h>

void __global__
updateFaces(Triangle *g_tris, uint *g_collapseTargets, bool *g_isTriangleValid, uint numTris){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numTris) {
        return;
    }
    Triangle t = g_tris[idx];
    t.v1 = g_collapseTargets[t.v1];
    t.v2 = g_collapseTargets[t.v2];
    t.v3 = g_collapseTargets[t.v3];
    g_isTriangleValid[idx] = (t.v1 != t.v2 && t.v2 != t.v3 && t.v1 != t.v3);
    g_tris[idx] = t;
}

void __global__
updateEdges(Edge *g_edges, uint *g_collapseTargets, uint numEdges){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }
    Edge e = g_edges[idx];
    uint v1 = g_collapseTargets[e.v1],
         v2 = g_collapseTargets[e.v2];
    e.v1 = MIN(v1, v2);
    e.v2 = MAX(v1, v2);
    e.isValid = (e.v1 != e.v2);
    g_edges[idx] = e;
}


extern "C"
void updateIndices(Edge *d_edges, uint numEdges, Triangle *d_tris, uint numTris, uint *d_collapseTargets,
                   bool *d_isTriangleValid, uint threadsPerBlock){

    uint blocksInGrid = getBlocksInGrid(numEdges, threadsPerBlock);
    dim3 grid(blocksInGrid,1,1);
    dim3 threads(threadsPerBlock,1,1);
    updateEdges<<<grid, threads>>>(d_edges, d_collapseTargets,  numEdges);
    checkCudaErrors(cudaGetLastError());

    blocksInGrid = getBlocksInGrid(numTris, threadsPerBlock);
    grid = dim3(blocksInGrid,1,1);
    threads = dim3(threadsPerBlock,1,1);
    updateFaces<<<grid, threads>>>(d_tris, d_collapseTargets, d_isTriangleValid, numTris);
    checkCudaErrors(cudaGetLastError());
}