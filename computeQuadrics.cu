// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>
#include <helper_math.h>

//Thrust
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <thrust/unique.h>

#include <defines.h>

void __device__
calcFaceQuadric(Quadric &quad, const float3 &a, const float3 &b, const float3 &c) {
    Pos ab = b - a ,
        ac = c - a; 

    Pos n = {ab.y * ac.z - ac.y * ab.z, ac.x * ab.z - ab.x * ac.z, ab.x * ac.y - ac.x * ab.y};
    n = normalize(n);
    float p[4] = {n.x, n.y, n.z, -dot(a, n)};

#pragma unroll
    for (uint i = 0; i < 4; i++){
#pragma unroll
        for (uint j = 0; j <= i; j++){
            quad(i,j) = p[i]*p[j];
        }
    }

    //OPT
    // Note about parallel binning (GDG11) (instead of atomic add?)

}

void __device__
calcSingleEdgeQuadric(Quadric &q, Pos v1, Pos v2, Pos v3) {
    // a2 = (v3 - v1, v1 - v2) / ||v1-v2||^2
    // n = (v3 - v1) + a2*(v2 - v1)
    // n' = n / ||n||
    // d = - (n,v1)
    // return [n d]
    
    float l = length(v1 - v2);
    float a2 = dot(v3 - v1, v1 - v2) / (l*l);
    Pos nn = normalize((v3 - v1) + a2 * (v2 - v1));
    float d =  - dot(nn, v1);


    float p[4] = {nn.x, nn.y, nn.z, d};
#pragma unroll
    for (uint i = 0; i < 4; i++){
#pragma unroll
        for (uint j = 0; j <= i; j++){
            q(i,j) = p[i]*p[j];
        }
    }
}
void __global__ calcFaceQuadrics(Triangle *g_tris, float3 *g_pos, Quadric *g_quadrics, uint numTris){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numTris) {
        return;
    }
    Triangle tri = g_tris[idx];
    Quadric quad;
    calcFaceQuadric(quad, g_pos[tri.v1], g_pos[tri.v2], g_pos[tri.v3]);
    
    for (uint i = 0; i < 10; i++){
        atomicAdd(&g_quadrics[tri.v1].m[i], quad.m[i]);
        atomicAdd(&g_quadrics[tri.v2].m[i], quad.m[i]);
        atomicAdd(&g_quadrics[tri.v3].m[i], quad.m[i]);
    }
    
}


void __global__ calcSingleEdgeQuadrics(Edge *g_edges, float3 *g_pos, Quadric *g_quadrics, uint numEdges){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numEdges) {
        return;
    }
    Edge e= g_edges[idx];
    if (!e.isSingle) {
        return;
    }
    Quadric quad;
    calcSingleEdgeQuadric(quad, g_pos[e.v1], g_pos[e.v2], g_pos[e.opposite]);
    
    for (uint i = 0; i < 10; i++){
        atomicAdd(&g_quadrics[e.v1].m[i], quad.m[i]);
        atomicAdd(&g_quadrics[e.v2].m[i], quad.m[i]);
    }
    
}

extern "C"
void computeQuadrics(Triangle *d_tris, uint numTris, Edge *d_edges, uint numEdges, float3 *d_pos, uint numVerts, Quadric *d_quadrics, float *d_planes, uint threadsPerBlock){
    
    uint blocksInGrid = getBlocksInGrid(numTris, threadsPerBlock);
    dim3 grid(blocksInGrid,1,1);
    dim3 threads(threadsPerBlock, 1,1);
    calcFaceQuadrics<<<grid, threads>>>(d_tris, d_pos, d_quadrics, numTris);
    checkCudaErrors(cudaGetLastError());

    blocksInGrid = getBlocksInGrid(numEdges, threadsPerBlock);
    grid = dim3(blocksInGrid);
    threads = dim3(threadsPerBlock);
    calcSingleEdgeQuadrics<<<grid, threads>>>(d_edges, d_pos, d_quadrics, numEdges);
    checkCudaErrors(cudaGetLastError());
}

