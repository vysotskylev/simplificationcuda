#include <iostream>
#include <limits>
#include <string>
#include <ctime>


#include <cuda_runtime.h>
#include <vector_types.h>
#include <vector_functions.h>

#include <helper_cuda.h>    // includes cuda.h and cuda_runtime_api.h
#include <helper_functions.h>

//My defines
#include <defines.h> 

#include <Simplifier.h>

//Ply reading
#include "ply/ply.h"

//---------------- External kernel launchers ------------------------
//extern "C"
//void buildEdges(Triangle *d_tris, uint numTris, Edge *d_edges, uint threadsPerBlock);
//
//// Returns new number of edges (= of unique edges)
//extern "C"
//uint compactEdges(Edge *d_edges, uint numEdges, uint threadsPerBlock);
//
//extern "C"
//uint compactInvalidEdges(Edge *d_edges, uint numEdges, uint threadsPerBlock);
//
//extern "C"
//void compactFacesAndVertices(Triangle *d_tris, uint numTris, Pos *d_pos,  uint numVerts,
//                        bool *d_isTriangleValid, bool *d_isVertexValid,
//                        uint &outNewTris, uint &outNewVerts, uint threadsPerBlock) ;
//
//extern "C"
//void computeQuadrics(Triangle *d_tris, uint numTris, Edge *d_edges, uint numEdges, float3 *d_pos, uint numVerts, Quadric *d_quadrics, float *d_planes, uint threadsPerBlock);
//
//extern "C"
//void fillArray(float *d_array, uint numEls, float value);
//
//extern "C"
//void optimizeCollapses(Pos *d_pos, Edge *d_edges, uint numEdges, Quadric *d_quadrics, Pos *d_optimumPos, 
//                       float *d_edgeCost, float *d_vertexCost, float maxError, bool *d_willCollapseEdge, uint threadsPerBlock);
//
//extern "C"
//void collapseEdges(Pos* d_pos, Edge *d_edges, uint numEdges, bool *d_willCollapseEdge, Quadric *d_quadrics, uint numVerts, Pos *d_optimumPos, 
//                       float *d_edgeCost, float *d_vertexCost, uint *d_collapseTargets, bool *d_isVertexValid,
//                       uint *d_edgeIDs, uint threadsPerBlock);
//
//extern "C"
//void updateIndices(Edge *d_edges, uint numEdges, Triangle *d_tris, uint numTris, uint *d_collapseTargets,
//                  bool *d_isTriangleValid, uint threadsPerBlock);

//----------------- Forward declarations ----------------------------
//void init();
//void finalize();
//uint simplify(Pos *d_pos, uint numVerts, Triangle *d_tris, uint numTris, float maxError);
bool read_file(const char *fname, Triangle *&h_tris, uint &outNumTris, Pos *&h_pos, uint &outNumVerts);
bool write_file(const char *fname, Triangle *h_tris, uint numTris, Pos *h_pos, uint numVerts);
double get_wall_time();
double get_cpu_time();

//------------------- Constant parameters -------------------------
//const uint COMPACT_AFTER_ITERS = 1;
//const uint MAX_NUM_ITERS = 300;
//const uint THREADS_PER_BLOCK = 256;
//------------------- Global variables ----------------------------
//Pos *d_pos = 0;
//Triangle *d_tris = 0;
//Edge *d_edges = 0;
//Quadric *d_quadrics = 0;
//float *d_planes = 0;
//Pos *d_optimumPos = 0;
//float *d_edgeCost = 0,
//      *d_vertexCost = 0;
//uint *d_edgeIDs = 0;
//uint *d_collapseTargets = 0;
//bool *d_isVertexValid = 0,
//     *d_isTriangleValid = 0;
//bool *d_willCollapseEdge = 0;
//
//uint numTris, numVerts, numEdges;
int main(int argc, char **argv){
    using std::cout;
    using std::endl;
    using std::cerr;

    Simplifier simplifier;
    simplifier.initialize();


    Pos *h_pos;
    Triangle *h_tris;
    
    if (argc < 4) {
        cerr << "Usage: simplify.exe in.ply out.ply max_error" << endl;
    }

    char *inFileName = argv[1],
         *outFileName = argv[2];
    float maxLinearError = std::stof(argv[3]);

    std::cout << "Reading file...";
    double tprev = get_wall_time(),
            tcur;
    uint numTris, numVerts;
    if (!read_file(inFileName, h_tris, numTris, h_pos, numVerts)){
        return 1;
    }
    tcur = get_wall_time();
    cout << "Done in " << tcur - tprev << "secs " << endl;
    
    std::vector<Pos> vertices(h_pos, h_pos + numVerts);
    std::vector<Triangle> triangles(h_tris, h_tris + numTris);

    std::vector<Pos> outVertices;
    std::vector<Triangle> outTriangles;

    tprev = get_wall_time();
    cout << "Simplifying...";
    simplifier.simplify(vertices, triangles, maxLinearError, outVertices, outTriangles);
    cout << "Done in " << get_wall_time() - tprev << "secs " << endl;
    
    cout << "Writing file...";
    tprev = get_wall_time();
    if (!write_file(outFileName, outTriangles.data(), outTriangles.size(), outVertices.data(), outVertices.size())) {
        return 1;
    }
    
    tcur = get_wall_time();
    cout << "Done in " << tcur - tprev << "secs " << endl;

   // std::cout << "Setting up GPU...";
   // init();
  
   //  tprev = get_wall_time();
   // 
   // checkCudaErrors(cudaMemcpy(d_pos, h_pos, numVerts*sizeof(*d_pos), cudaMemcpyHostToDevice));
   // checkCudaErrors(cudaMemcpy(d_tris, h_tris, numTris*sizeof(*d_tris), cudaMemcpyHostToDevice));
   // tcur = get_wall_time();
   // cout << "Done in " << tcur - tprev << "secs " << endl;

   // tprev = get_wall_time();
   // cout << "Simplifying(maxError = "<<maxError << ")...";
   // uint uniqueEdges = simplify(d_pos, numVerts, d_tris, numTris, maxError);

   //// checkCudaErrors(cudaDeviceSynchronize());
   // tcur = get_wall_time();
   // cout << "Done in " << tcur - tprev << "secs " << endl;;
   // 
   // //DEBUG
   // bool *h_isTriangleValid = new bool[numTris];
   // checkCudaErrors(cudaMemcpy(h_isTriangleValid, d_isTriangleValid, numTris*sizeof(*d_isTriangleValid), cudaMemcpyDeviceToHost));
   // 
   // uint counter = 0;
   // for (uint i = 0; i < numTris; i++){
   //     if (!h_isTriangleValid[i]) {
   //         counter++;
   //     }
   // }
   // if (counter == 0) {
   //     std::cerr << "All triangles valid: problem!" <<std::endl;
   // }

   // uint newNumTris = numTris, newNumVerts = numVerts;
   // compactFacesAndVertices(d_tris, numTris, d_pos, numVerts, d_isTriangleValid, d_isVertexValid, newNumTris, newNumVerts, THREADS_PER_BLOCK);
   // 
    ////DEBUG
    //buildEdges(d_tris, newNumTris, d_edges, THREADS_PER_BLOCK);
    //
    //std::cout << "Were edges: " << newNumTris * 3 << std::endl;
    //compactEdges(d_edges, 3*newNumTris, THREADS_PER_BLOCK);
    //std::cout << "Now: " << uniqueEdges << std::endl;
    //
    //checkCudaErrors(cudaMemcpy(h_edges, d_edges, numEdges*sizeof(*d_edges), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_quadrics, d_quadrics, numVerts*sizeof(*d_quadrics), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_optimumPos, d_optimumPos, uniqueEdges*sizeof(*d_optimumPos), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_edgeCost, d_edgeCost, uniqueEdges*sizeof(*d_edgeCost), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_collapseTargets, d_collapseTargets, numVerts*sizeof(*d_collapseTargets), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_vertexCost, d_vertexCost, numVerts*sizeof(*d_vertexCost), cudaMemcpyDeviceToHost));
    
    //checkCudaErrors(cudaMemcpy(h_pos, d_pos, newNumVerts*sizeof(*d_pos), cudaMemcpyDeviceToHost));
    //checkCudaErrors(cudaMemcpy(h_tris, d_tris, newNumTris*sizeof(*d_tris), cudaMemcpyDeviceToHost));


   /* delete [] h_tris;
    delete [] h_pos;*/
    /*for (uint i = 0; i < newNumTris; i++) {
        Triangle t = h_tris[i];
        cout << t.v1 << ' ' << t.v2 << ' ' << t.v3 << endl;
    }
    for (uint i = 0; i < newNumVerts; i++){
        Pos p = h_pos[i];
        cout << p.x << ' ' << p.y << ' ' << p.z << endl;
    }*/
    ////DEBUG
    //for (uint i = 0; i < uniqueEdges; i++){
    //    Pos op = h_optimumPos[i];
    //    cout << "Edge (" << h_edges[i].v1 << ", " << h_edges[i].v2 << "): " <<
    //        op.x << " " << op.y << " " << op.z << ", cost: " << h_edgeCost[i] << endl;
    //}
    
    //DEBUG
    //checkCudaErrors(cudaMemcpy(h_planes, d_planes, numTris*4*sizeof(float), cudaMemcpyDeviceToHost));

    //DEBUG
    //float *p = h_planes + 12;
    //for (uint i = 0; i < 4; i++){
    //    std::cout << p[i] << ' ' ;
    //}
    //std::cout << std::endl;
    //
    //Quadric q = h_quadrics[0];
    //for (uint i = 0; i < 4; i++){
    //    for (uint j = 0; j < 4; j++){
    //        std::cout << q(MAX(i,j), MIN(i,j)) << ' ';
    //    }
    //    std::cout << std::endl;
    //}

    //finalize();

    ////ply reading test
    //Triangle *h_tris1 = 0;
    //Pos *h_pos1 = 0;
    //uint numVerts1, numTris1;
    //read_file("data/cube.ply", h_tris1, numTris1, h_pos1, numVerts1);
    //for (uint i = 0; i < numTris1; i++) {
    //    Triangle t = h_tris1[i];
    //    cout << t.v1 << ' ' << t.v2 << ' ' << t.v3 << endl;
    //}
    //for (uint i = 0; i < numVerts1; i++){
    //    Pos p = h_pos1[i];
    //    cout << p.x << ' ' << p.y << ' ' << p.z << endl;
    //}
    //std::cout << write_file("data/new_cube.ply", h_tris1, numTris1, h_pos1, numVerts1);
    return 0;
}
//
//uint simplify(Pos *d_pos, uint numVerts, Triangle *d_tris, uint numTris, float maxError) {
//    ////DEBUG
//    //Edge *h_edges = new Edge[numEdges];
//    //Pos *h_pos = new Pos[numVerts];
//    //checkCudaErrors(cudaMemcpy(h_pos, d_pos, numVerts*sizeof(*d_pos), cudaMemcpyDeviceToHost));
//    //checkCudaErrors(cudaMemcpy(h_edges, d_edges, numEdges*sizeof(*d_edges), cudaMemcpyDeviceToHost));
//
//    buildEdges(d_tris, numTris, d_edges, THREADS_PER_BLOCK);   
//     
//    std::cout << "Were edges: " << numEdges << std::endl;
//    uint uniqueEdges = compactEdges(d_edges, numEdges, THREADS_PER_BLOCK);
//    std::cout << "Now: " << uniqueEdges << std::endl;
//     
//    checkCudaErrors(cudaMemset(d_quadrics, 0, numVerts*sizeof(*d_quadrics)));
//    computeQuadrics(d_tris, numTris, d_edges, numEdges,  d_pos, numVerts, d_quadrics, d_planes, THREADS_PER_BLOCK);
//
//    checkCudaErrors(cudaMemset(d_isVertexValid, 1, numVerts*sizeof(*d_isVertexValid)));
//    checkCudaErrors(cudaMemset(d_isTriangleValid, 1, numTris*sizeof(*d_isTriangleValid)));
//
//    for (uint numIter = 1; numIter <= MAX_NUM_ITERS; numIter++) {
//        fillArray(d_vertexCost, numVerts, std::numeric_limits<float>::infinity());
//         
//        checkCudaErrors(cudaMemset(d_willCollapseEdge, 0, uniqueEdges*sizeof(*d_willCollapseEdge)));
//        optimizeCollapses(d_pos, d_edges, uniqueEdges, d_quadrics, d_optimumPos, d_edgeCost, d_vertexCost, maxError, d_willCollapseEdge, THREADS_PER_BLOCK);
//        collapseEdges(d_pos, d_edges, uniqueEdges, d_willCollapseEdge, d_quadrics, numVerts, d_optimumPos, d_edgeCost, d_vertexCost, d_collapseTargets, d_isVertexValid, d_edgeIDs, THREADS_PER_BLOCK);
//        updateIndices(d_edges, uniqueEdges, d_tris, numTris, d_collapseTargets,  d_isTriangleValid, THREADS_PER_BLOCK);
//    
//        //DEBUG
//        //std::cout << "iteration #" << numIter<< std::endl;
//        //checkCudaErrors(cudaMemcpy(h_edges, d_edges, numEdges*sizeof(*d_edges), cudaMemcpyDeviceToHost));
//        //for (uint i = 0; i < uniqueEdges; i++){
//        //    std::cout << h_edges[i].v1 << ' ' << h_edges[i].v2 << std::endl;
//        //}
//        //ENDDEBUG
//
//        if (numIter % COMPACT_AFTER_ITERS == 0) {
//            uint newNum = compactInvalidEdges(d_edges, uniqueEdges, THREADS_PER_BLOCK);
//            std::cout << "newNum: " << newNum << std::endl;
//            if (newNum == uniqueEdges) {
//                std::cout << "No more collapses" << std::endl;
//                // No collapses occured during last iteration
//                break;
//            }
//            uniqueEdges = newNum;
//        }
//    }
//    return uniqueEdges;
//
//}

//void init(){
//    checkCudaErrors(cudaMalloc((void **) &d_pos, numVerts*sizeof(*d_pos)));
//    checkCudaErrors(cudaMalloc((void **) &d_tris, numTris*sizeof(*d_tris)));
//
//    checkCudaErrors(cudaMalloc((void **) &d_edges, numEdges*sizeof(*d_edges)));
//
//    checkCudaErrors(cudaMalloc((void **) &d_quadrics, numVerts*sizeof(*d_quadrics)));
//    checkCudaErrors(cudaMalloc((void **) &d_optimumPos, numEdges*sizeof(*d_optimumPos)));
//    checkCudaErrors(cudaMalloc((void **) &d_edgeCost, numEdges*sizeof(*d_edgeCost)));
//    checkCudaErrors(cudaMalloc((void **) &d_vertexCost, numVerts*sizeof(*d_vertexCost)));
//
//    checkCudaErrors(cudaMalloc((void **) &d_willCollapseEdge, numEdges*sizeof(*d_willCollapseEdge)));
//
//    checkCudaErrors(cudaMalloc((void **) &d_isVertexValid, numVerts*sizeof(*d_isVertexValid)));
//    checkCudaErrors(cudaMalloc((void **) &d_isTriangleValid, numTris*sizeof(*d_isTriangleValid)));
//
//    checkCudaErrors(cudaMalloc((void **) &d_collapseTargets, numVerts*sizeof(*d_collapseTargets)));
//    checkCudaErrors(cudaMalloc((void **) &d_edgeIDs, numVerts*sizeof(*d_edgeIDs)));
//
//    //DEBUG
//    checkCudaErrors(cudaMalloc((void **) &d_planes, numTris*4*sizeof(float)));
//}
//
//void finalize(){
//    checkCudaErrors(cudaFree(d_pos));
//    checkCudaErrors(cudaFree(d_tris));
//    
//    checkCudaErrors(cudaFree(d_edges));
//    
//    checkCudaErrors(cudaFree(d_quadrics));
//    checkCudaErrors(cudaFree(d_optimumPos));
//    checkCudaErrors(cudaFree(d_edgeCost));
//    checkCudaErrors(cudaFree(d_vertexCost));
//
//    checkCudaErrors(cudaFree(d_willCollapseEdge));
//
//    checkCudaErrors(cudaFree(d_isVertexValid));
//    checkCudaErrors(cudaFree(d_isTriangleValid));
//
//    checkCudaErrors(cudaFree(d_collapseTargets));
//    checkCudaErrors(cudaFree(d_edgeIDs));
//
//    //DEBUG
//    checkCudaErrors(cudaFree(d_planes));
//}

typedef struct Face {
  unsigned char nverts;    /* number of vertex indices in list */
  uint *verts;              /* vertex index list */
  void *other_props;       /* other properties */
} Face;

typedef struct Vertex {
  float x,y,z;
  void *other_props;       /* other properties */
} Vertex;

bool
read_file(const char *fname, Triangle *&h_tris, uint &outNumTris, Pos *&h_pos, uint &outNumVerts)
{
    PlyFile *in_ply;
    PlyOtherProp *vert_other,*face_other;

    PlyProperty vert_props[] = { /* list of property information for a vertex */
        {"x", Float32, Float32, offsetof(Vertex,x), 0, 0, 0, 0},
        {"y", Float32, Float32, offsetof(Vertex,y), 0, 0, 0, 0},
        {"z", Float32, Float32, offsetof(Vertex,z), 0, 0, 0, 0}
    };

    PlyProperty face_props[] = { /* list of property information for a face */
        {"vertex_indices", Uint32, Uint32, offsetof(Face,verts),
        1, Uint8, Uint8, offsetof(Face,nverts)},
    };


    FILE *file = fopen(fname, "r");
    if (!file) {
        std::cerr << "Couldn't open file " << fname <<std::endl;
        return false;
    }
    
    int elem_count;
    char *elem_name;


    /*** Read in the original PLY object ***/

    in_ply = read_ply (file);
    if (!in_ply){ 
        std::cerr << "Cannot read ply file (possibly wrong structure)" << std::endl;
        return false;
    }
    /* examine each element type that is in the file (vertex, face) */

    for (int i = 0; i < in_ply->num_elem_types; i++) {

        /* prepare to read the i'th list of elements */
        elem_name = setup_element_read_ply (in_ply, i, &elem_count);

        if (equal_strings ("vertex", elem_name)) {
            outNumVerts = elem_count;
            h_pos = new Pos[outNumVerts];

            /* set up for getting vertex elements */
            /* (we want x,y,z) */

            setup_property_ply (in_ply, &vert_props[0]);
            setup_property_ply (in_ply, &vert_props[1]);
            setup_property_ply (in_ply, &vert_props[2]);

           
            get_other_properties_ply (in_ply, 
					     offsetof(Vertex,other_props));
            Vertex v;
            for (int j = 0; j < elem_count; j++) {
                get_element_ply (in_ply, (void *) &v);
                h_pos[j].x = v.x;
                h_pos[j].y = v.y;
                h_pos[j].z = v.z;
            }
        }
        else if (equal_strings ("face", elem_name)) {

            /* create a list to hold all the face elements */
            
            outNumTris = elem_count;
            h_tris = new Triangle[outNumTris];

            /* set up for getting face elements */
            /* (all we need are vertex indices) */

            setup_property_ply (in_ply, &face_props[0]);
            face_other = get_other_properties_ply (in_ply, 
                offsetof(Face,other_props));

            /* grab all the face elements and place them in our list */

            for (int j = 0; j < elem_count; j++) {
                Face f;
                //TODO check for possible memory leak
                get_element_ply (in_ply, (void *) &f); 
                if (f.nverts != 3) {
                    std::cerr << "All faces in mesh must be triangles" << std::endl;
                    return false;
                }
                h_tris[j].v1 = f.verts[0];
                h_tris[j].v2 = f.verts[1];
                h_tris[j].v3 = f.verts[2];
            }
        }
        else  /* all non-vertex and non-face elements are grabbed here */
            get_other_element_ply (in_ply);
    }

    /* close the file */
    /* (we won't free up the memory for in_ply because we will use it */
    /*  to help describe the file that we will write out) */

    close_ply (in_ply);
    free_ply( in_ply);
    fclose(file);
    return true;
}

bool 
write_file(const char *fname, Triangle *h_tris, uint numTris, Pos *h_pos, uint numVerts)
{

    FILE *file = fopen(fname, "w");
    if (!file) {
        std::cerr << "Cannot open file " << fname << " for writing" << std::endl;
        return false;
    }

    PlyProperty vert_props[] = { /* list of property information for a vertex */
        {"x", Float32, Float32, offsetof(Pos,x), 0, 0, 0, 0},
        {"y", Float32, Float32, offsetof(Pos,y), 0, 0, 0, 0},
        {"z", Float32, Float32, offsetof(Pos,z), 0, 0, 0, 0}
    };

    PlyProperty face_props[] = { /* list of property information for a face */
        {"vertex_indices", Uint32, Uint32, offsetof(Face,verts),
        1, Uint8, Uint8, offsetof(Face,nverts)},
    };

    char *elist[] = {"vertex", "face"};
    
    PlyFile *ply = write_ply (file, 2, elist, PLY_ASCII);
    if (!ply) {
        std::cerr << "Cannot setup ply writer" <<std::endl;
        return false;
    }
    /* describe what properties go into the vertex elements */
    /* (position x,y,z and normals nx,ny,nz if they were provided) */

    describe_element_ply (ply, "vertex", numVerts);
    describe_property_ply (ply, &vert_props[0]);
    describe_property_ply (ply, &vert_props[1]);
    describe_property_ply (ply, &vert_props[2]);


    /* describe face properties (just list of vertex indices) */
    describe_element_ply (ply, "face", numTris);
    describe_property_ply (ply, &face_props[0]);

    /* we've told the routines enough information so that the file header */
    /* can be written out now */
    header_complete_ply (ply);

    /* set up and write the vertex elements */
    put_element_setup_ply (ply, "vertex");
    for (uint i = 0; i < numVerts; i++)
        put_element_ply (ply, (void *) &h_pos[i]);

    /* set up and write the face elements */
    put_element_setup_ply (ply, "face");
    for (uint i = 0; i < numTris; i++){
        Triangle t = h_tris[i];
        uint vs[3] = {t.v1, t.v2, t.v3};
        Face f = {3, vs};
        put_element_ply (ply, (void *) &f);
    }

    /* close the file and free up the memory */

    close_ply (ply);
    free_ply (ply);
    return true;
}


//  Windows
#ifdef _WIN32
#include <Windows.h>
double get_wall_time(){
    LARGE_INTEGER time,freq;
    if (!QueryPerformanceFrequency(&freq)){
        //  Handle error
        return 0;
    }
    if (!QueryPerformanceCounter(&time)){
        //  Handle error
        return 0;
    }
    return (double)time.QuadPart / freq.QuadPart;
}
double get_cpu_time(){
    FILETIME a,b,c,d;
    if (GetProcessTimes(GetCurrentProcess(),&a,&b,&c,&d) != 0){
        //  Returns total user time.
        //  Can be tweaked to include kernel times as well.
        return
            (double)(d.dwLowDateTime |
            ((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
    }else{
        //  Handle error
        return 0;
    }
}

//  Posix/Linux
#else
#include <sys/time.h>
double get_wall_time(){
    struct timeval time;
    if (gettimeofday(&time,NULL)){
        //  Handle error
        return 0;
    }
    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
double get_cpu_time(){
    return (double)clock() / CLOCKS_PER_SEC;
}
#endif