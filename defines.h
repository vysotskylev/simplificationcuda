#pragma once

#include <vector_types.h>
#include <vector_functions.h>


typedef unsigned int uint;

typedef struct {
    uint v1, v2, opposite;
    bool isSingle, isValid;
} Edge;

struct Quadric {
    float m[10];
    
    inline __host__ __device__ float &
    operator() (uint i, uint j) {
        return m[i * (i + 1) / 2 + j];
    }

    inline __host__ __device__ float
    operator() (uint i, uint j) const {
        return m[i * (i + 1) / 2 + j];
    } 
    inline __host__ __device__
    Quadric &operator+= (const Quadric &o){ 
        for (uint i = 0 ; i < 10; i++){
            m[i] += o.m[i];
        }
        return *this;
    }
};

typedef float3 Pos;
struct Triangle {
    uint v1, v2, v3;
};

inline uint
getBlocksInGrid(uint n, uint threadsPerBlock) {
    return (n / threadsPerBlock) + (n % threadsPerBlock == 0 ? 0 : 1);
}