// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>


//Thrust
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <thrust/unique.h>

#include <defines.h>

void __global__ kernel(Triangle *g_tris, Edge *g_edges, uint numTris){
    const uint idx = blockDim.x * blockIdx.x + threadIdx.x;
    if (idx >= numTris) {
        return;
    }
    Triangle tri = g_tris[idx];
    Edge e1 = {MIN(tri.v1, tri.v2), MAX(tri.v1, tri.v2), tri.v3, false, true},
         e2 = {MIN(tri.v2, tri.v3), MAX(tri.v2, tri.v3), tri.v1, false, true},
         e3 = {MIN(tri.v1, tri.v3), MAX(tri.v1, tri.v3), tri.v2, false, true};
    g_edges[3 * idx + 0] = e1;
    g_edges[3 * idx + 1] = e2;
    g_edges[3 * idx + 2] = e3;
    //OPT maybe better to launch this kernel for each edge?
}

extern "C"
void buildEdges(Triangle *d_tris, uint numTris, Edge *d_edges, uint threadsPerBlock){
    uint blocksPerGrid = getBlocksInGrid(numTris, threadsPerBlock);
    dim3 grid(blocksPerGrid,1,1);
    dim3 threads(threadsPerBlock, 1,1);
    kernel<<<grid, threads>>>(d_tris, d_edges, numTris);
    checkCudaErrors(cudaGetLastError());
}

